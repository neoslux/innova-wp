<?php
/**
 * Created by PhpStorm.
 * User: Alexandre
 * Date: 3/6/2018
 * Time: 12:46 PM
 */

/* Template Name: realisations */

get_header();
$cat = strip_tags($_GET['cat']);

if($cat>0) {
	$images = show_img($cat,9);
} else {
	$images = show_img(2,9);
};

?>

<header class="header-area" id="sticky-header">
	<nav class="navbar navbar-expand-lg navbar-light">
		<a class="navbar-brand" href="<?= get_site_url(); ?>"><img src="<?= get_template_directory_uri()?>/img/logo.svg" alt="" class="logo-main"></a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
			<span class="navbar-toggler-icon"></span>
		</button>

		<?php wp_nav_menu( array(
					'menu' => 'innova_menu',
					'theme_location' => 'header-menu',
					'container' => 'div',
					'container_class' => 'collapse navbar-collapse justify-content-end',
					'container_id' => 'collapsibleNavbar',
					'menu_class' => 'navbar-nav'
		) ); ?>

	</nav>
	<div class="row blogTitle">
		<h2 class="center">Nos Réalisations</h2>
	</div>
</header>

<div class="container-fluid">
	<div class="row">
		<ul class="nav col-12 justify-content-center realisation-nav-div">
			<li class="nav-item col-12 col-sm-4 text-center">
				<a class="nav-link <?php if(!isset($_GET['cat']) || $_GET['cat'] === '2'): ?>activeCat<?php endif; ?>" href="<?= get_site_url() ?>/realisations/?cat=2">Construction <span class="glyphicon glyphicon-menu-right"></span></a>
			</li>
			<li class="nav-item col-12 col-sm-4 text-center">
				<a class="nav-link <?php if(isset($_GET['cat']) && $_GET['cat'] === '3'): ?>activeCat<?php endif; ?>" href="<?= get_site_url() ?>/realisations/?cat=3">Rénovation</a>
			</li>
			<li class="nav-item col-12 col-sm-4 text-center">
				<a class="nav-link <?php if(isset($_GET['cat']) && $_GET['cat'] === '4'): ?>activeCat<?php endif; ?>" href="<?= get_site_url() ?>/realisations/?cat=4">Consulting</a>
			</li>
		</ul>
	</div>
	<div class="row content">
		<div class="col-12">
			<!-- Reapeat realisations images -->
            <?php if ($images != NULL) : ?>
                <?php foreach ($images as $key=>$value) : ?>
                    <div class="col-12 col-sm-6 col-xl-4 img-container">
                        <?php echo $value['img'] ; ?>
                        <div class="middle">
                            <?php echo $value['caption'] ; ?>
                        </div>
                    </div>
                <?php endforeach ; ?>
            <?php else : ?>
                <div class="col-12 col-sm-6 col-xl-4 img-container">
						<img src="<?= get_template_directory_uri() ?>/img/innova_logo.jpg" class="img-fluid image">
						<div class="middle">
							<div class="text">Pas d'image...</div>
						</div>
                </div>
            <?php endif; ?>
			<!-- End reapeat -->
		</div>
	</div>
</div>

<?php get_footer(); ?>