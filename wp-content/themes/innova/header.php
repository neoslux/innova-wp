<?php
/**
 * Created by PhpStorm.
 * User: jadmin
 * Date: 20/02/2018
 * Time: 13:53
 */
?>

<!doctype html>
<html>

<head>
	<!-- title -->
	<title>
		<?php bloginfo('name'); ?>
		<?php if (is_home() || is_front_page()) : ?>
			<?php bloginfo('description'); ?>
		<?php else : ?>
			<?php wp_title("", true); ?>
		<?php endif; ?>
	</title>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	      integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<?= get_template_directory_uri()?>/style.css">
	<link href="<?= get_template_directory_uri()?>/css/lightbox.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?= get_template_directory_uri()?>/slick-1.8.0/slick/slick.css"/>
    <link rel="stylesheet" type="text/css" href="<?= get_template_directory_uri()?>/slick-1.8.0/slick/slick-theme.css"/>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>

    <!-- pas the site url in the script -->
    <script type="text/javascript">var homeUrl = '<?= get_home_url(); ?>';</script>
	<!-- Lightbox -->
	<script src="<?= get_template_directory_uri();?>/js/lightbox-plus-jquery.js"></script>

	<!-- wp head includes -->
	<?php wp_head();  ?>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
</head>

<body>