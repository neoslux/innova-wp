<?php
/**
 * Created by PhpStorm.
 * User: Alexandre
 * Date: 2/20/2018
 * Time: 5:27 PM
 */

class neos_carousel {

	// types of galery :
	// 4 = consulting
	// 2 = construction
	// 3 = renovation
	var $type ;
	var $qty ;

	function __construct($type,$qty) {
		$this->type = $type ;
		$this->qty = $qty ;
	}

	// get the realisations first image url $ caption in an array from the tinycarousel db
	 static function get_first_image($type,$qty) {
	 	try{
			global $wpdb ;
			$realisations = $wpdb->get_results("SELECT img_imageurl, img_title, MIN(img_id) FROM {$wpdb->prefix}tinycarousel_image WHERE img_gal_id={$type} GROUP BY img_title LIMIT $qty", ARRAY_A) ;
			$imagesArray = [] ;
			foreach ($realisations as $key=>$value) {
				$image = [] ;
				$image['url'] = $value["img_imageurl"] ;
				$image['caption'] = $value['img_title'] ;
				//$image['caption'] = $wpdb->get_results("SELECT post_excerpt FROM wp_posts WHERE guid='".$url."'", ARRAY_A) ;
				$imagesArray[] = $image ;
			}
			return $imagesArray ;
	    }catch (Exception $ex){
	 		error_log($ex->getMessage());
	 		return null;
	    }
	}

	public function display_first_image($type,$qty) {
		$imagesArray = self::get_first_image($type, $qty) ;
		$htmlArray = [] ;
		$html = [] ;
		foreach ($imagesArray as $key=>$value) {
			$html['img']        = '<img src="' . $value['url'] . '" class="img-fluid image" alt="' . $value['caption'] .'" >' ;
			$html['caption'] = '<div class="text">' . $value['caption'] . '</div>' ;
			$htmlArray[] = $html;
		} ;
		return $htmlArray ;
	}

	public function get_other_images_url($title) {
		try {
			global $wpdb ;
			$urlArray = $wpdb->get_results("SELECT img_imageurl FROM {$wpdb->prefix}tinycarousel_image WHERE img_title='" . $title . "' LIMIT 6", ARRAY_A) ;
		}catch (Exception $ex){
			error_log($ex->getMessage());
			return null;
		}
		return $urlArray ;
	}

}