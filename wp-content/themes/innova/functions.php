<?php

/* ----- general ---------- */
// include the neos_carousel class
if(!class_exists("neos_carousel")){
	include_once( get_template_directory() . "/class/neos_carousel.php");
} ;

// add the thumbnails function to the theme and a specific size of image
add_theme_support('post-thumbnails');
add_image_size( 'homepage-thumb', 300, 200, true );
add_image_size( 'single-thumb', 600, 400, true );

// add a link to the single post around the image
function wpb_autolink_featured_images( $html, $post_id, $post_image_id ) {
	If (! is_singular()) {
		$html = '<a href="' . get_permalink( $post_id ) . '" title="' . esc_attr( get_the_title( $post_id ) ) . '">' . $html . '</a>';
		return $html;
	} else {
		return $html;
	}
}
add_filter( 'post_thumbnail_html', 'wpb_autolink_featured_images', 10, 3 );

/* ----- menu ---------- */

// Register the menu
function register_my_menu() {
	register_nav_menu('header-menu',__( 'innova_menu' ));
}
add_action( 'init', 'register_my_menu' );

// add contact in the menu
add_filter('wp_nav_menu_items','contact_function', 10, 2);
function contact_function( $nav, $args ) {
	$nav.='<li class="nav-item"><a class="nav-link" href="' . get_site_url() . '#contact">Contact</a></li>';
	return $nav;
};

/* ----- index page ---------- */

function show_img($type,$qty) {
	$galery = new neos_carousel($type,$qty) ;
	$img = $galery->display_first_image($type,$qty) ;
	return $img ;
} ;

function add_js_scripts() {
	wp_enqueue_script( 'script', get_template_directory_uri().'/js/script.js', array('jquery'), '1.0', true );

	// pass Ajax Url to script.js
	wp_localize_script('script', 'ajaxurl', admin_url( 'admin-ajax.php' ) );
}

add_js_scripts();
add_action( 'wp_ajax_neos_modal', 'neos_modal' );
add_action( 'wp_ajax_nopriv_neos_modal', 'neos_modal' );

function neos_modal() {

	$title = $_POST['title'];
	$galery = new neos_carousel(2,6) ;
	$url = $galery->get_other_images_url($title) ;
	$result = json_encode($url) ;
	echo $result ;
	die();
}

/* ----- blog page ---------- */

function get_articles($cat=0, $offset=0) {
	if ( have_posts() ) {
		$args = array(
			'numberposts' => 3,
			'offset' => $offset,
			'category' => $cat,
			'orderby' => 'post_date',
			'order' => 'DESC',
			'include' => '',
			'exclude' => '',
			'meta_key' => '',
			'meta_value' =>'',
			'post_type' => 'post',
			'post_status' => 'draft, publish, future, pending, private',
			'suppress_filters' => true
		);
		$posts = get_posts($arg) ;
		return $posts ;
	} else {
		return 'Aucun article... revenez plus tard ! ' ;
	}
}