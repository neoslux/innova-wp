<?php /* Template Name: index */ ?>
<?php get_header() ; ?>
<?php $images = show_img(2,6) ; ?>
<header class="header-area full-height" id="sticky-header">
	<div id="carousel" class="carousel slide" data-ride="carousel">
		<!-- Indicators under the slider -->
		<ul class="carousel-indicators">
			<li data-target="#carousel" data-slide-to="0" class="active"></li>
			<li data-target="#carousel" data-slide-to="1"></li>
			<li data-target="#carousel" data-slide-to="2"></li>
		</ul>
		<!-- Carousel images -->
		<div class="carousel-inner">
			<div class="carousel-item active">
				<img src="<?= get_template_directory_uri()?>/img/Fotolia_88069930_XXL_spg2dl_c_scale,w_1400.jpg" alt="First slide">
			</div>
			<div class="carousel-item">
				<img src="<?= get_template_directory_uri()?>/img/Fotolia_104713904_XL_z7fjvo_c_scale,w_1400.jpg" alt="Second slide">
			</div>
			<div class="carousel-item">
				<img src="<?= get_template_directory_uri()?>/img/Fotolia_91625390_L_qglswy_c_scale,w_1400.jpg" alt="Third slide">
			</div>
		</div>
		<!-- Left and right controls -->
		<a class="carousel-control-prev" href="#carousel" data-slide="prev">
			<span class="carousel-control-prev-icon"></span>
		</a>
		<a class="carousel-control-next" href="#carousel" data-slide="next">
			<span class="carousel-control-next-icon"></span>
		</a>
	</div>

	<nav class="navbar navbar-expand-lg navbar-dark">
		<a class="navbar-brand" href="<?= get_site_url(); ?>"><img src="<?= get_template_directory_uri()?>/img/logo.svg" alt="" class="logo-main"></a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
			<span class="navbar-toggler-icon"></span>
		</button>

		<?php wp_nav_menu( array(
			'menu' => 'innova_menu',
			'theme_location' => 'header-menu',
			'container' => 'div',
			'container_class' => 'collapse navbar-collapse justify-content-end',
			'container_id' => 'collapsibleNavbar',
			'menu_class' => 'navbar-nav',
		) ); ?>

	</nav>

	<div class="header-content">
		<div>
			<div class="header-link-div">
				<a role="button" class="btn btn-light front-btn" href="<?= get_site_url(); ?>/realisations/">Nos Réalisations</a>
				<a role="button" class="btn btn-light front-btn" href="#contact">Nous Contacter</a>
			</div>
		</div>
	</div>
</header>

<div class="container-fluid">
	<!-- Introduction Section -->
	<section class="introduction col-12" id="introduction">
		<h3>Qui sommes-nous ?</h3>
		<p>
            <?= do_shortcode("[contentblock id=1]")?>
        </p>
		<h4>Nos labels</h4>
		<!-- Repeat the 4 labels -->
		<div class="labels-div col-12">
			<div class="col-6">
				<img src="<?= get_template_directory_uri()?>/img/lux-t.png" class="img-fluid image">
			</div>
			<div class="col-6">
				<img src="<?= get_template_directory_uri()?>/img/seal-t.png"	class="img-fluid image  image">
			</div>
			<div class="col-6">
				<img src="<?= get_template_directory_uri()?>/img/membre.jpg" class="img-fluid image">
			</div>
			<div class="col-6">
				<img src="<?= get_template_directory_uri()?>/img/EFZ.png" class="img-fluid image">
			</div>
		</div>
	</section>
	<!-- End Introduction Section -->

	<!-- Start Realisation Section -->
	<section class="col-12" id="realisation">
		<div class="row">
            <h3><a href="<?= get_site_url(); ?>/realisations/">Nos réalisations</a></h3>
		</div>
		<div class="row">
			<ul class="nav col-12 justify-content-center realisation-nav-div">
				<li class="nav-item col-12 col-sm-4 text-center">
					<a class="nav-link" href="<?= get_site_url(); ?>/realisations/?cat=2">Construction <span class="glyphicon glyphicon-menu-right"></span></a>
				</li>
				<li class="nav-item col-12 col-sm-4 text-center">
					<a class="nav-link" href="<?= get_site_url(); ?>/realisations/?cat=3">Rénovation</a>
				</li>
				<li class="nav-item col-12 col-sm-4 text-center">
					<a class="nav-link" href="<?= get_site_url(); ?>/realisations/?cat=4">Consulting</a>
				</li>
			</ul>
		</div>

		<div class="row realisation-img-div justify-content-center">
			<!-- Repeat realisation images -->
            <?php foreach ($images as $key=>$value) : ?>
            <div class="col-12 col-sm-6 col-md-4 img-container">
                <?php echo $value['img'] ; ?>
                <div class="middle">
                    <?php echo $value['caption'] ; ?>
                </div>
            </div>
            <?php endforeach ; ?>

	<!-- End Realisation Section -->

	<!-- Start Blog Section -->
	<section class="col-12 blog-section" id="blog">
		<div class="row d-block text-center">
            <h3><a href="<?= get_site_url(); ?>/blog/">Blog</a></h3>
		</div>
		<div class="row d-block text-center">
			<h4>Nos derniers articles</h4>
		</div>

		<div class="row blog-div">
			<!-- Repeat 3 last blogs -->

                <?php
                $wp_query = new WP_Query();
                $wp_query->query('posts_per_page=3' . '&paged=1');
                while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
                    <div class="col-12 col-sm-6 col-md-4 blog-item">
                        <?php if(has_post_thumbnail()) {
                            the_post_thumbnail('homepage-thumb', ['class' => 'img-fluid']);
                        } else {
	                        echo('<img src="'. get_template_directory_uri() . '/img/innova_logo.jpg" class="img-fluid">');
                        }; ?>
                        <p><?= the_title(); ?></p>
                        <p><?php the_content('<br>Lire plus...', ''); ?></p>
                    </div>
                <?php endwhile; ?>
		</div>

	</section>
	<!-- End Blog Section -->

	<!-- Start Contact & Innova informations Section -->
	<section class="contact col-12" id="contact">
		<div class="row  mx-auto d-block text-center">
			<h3>Nous contacter</h3></div>
		<div class="row">
			<div class="col-md-6 col-xs-12 contact-form">
				<!-- Contact Form -->
				<?= do_shortcode("[contact-form-7 title=\"Contact form 1\"]")?>
				<!-- End Contact Form -->
			</div>
			<!-- Informations & Google Map -->
			<div class="col-md-6 col-xs-12 informations-part">
				<p><i class="fas fa-map-pin"></i><?= do_shortcode("[contentblock id=3]")?></p>
				<p><i class="fas fa-address-card"></i><?= do_shortcode("[contentblock id=4]")?></p>
				<p><i class="fas fa-phone"></i><?= do_shortcode("[contentblock id=5]")?></p>
				<p><i class="fas fa-mobile"></i><?= do_shortcode("[contentblock id=6]")?></p>

				<div id="googleMap" style="width:80%;height:250px;"></div>
			</div>
		</div>
	</section>
	<!-- End Contact & Innova informations Section -->
</div>

<?php get_footer() ; ?>
