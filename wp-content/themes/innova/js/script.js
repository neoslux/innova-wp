jQuery(function($) {
    // Add classes to the menu
    $('.navbar-nav > li').addClass('nav-item') ;
    $('.navbar-nav > li > a').addClass('nav-link') ;

    // Define the pathname = current href - homeUrl
    var fullUrl = $(location).attr('href') ;
    var length = homeUrl.length ;
    var path = fullUrl.substring(length) ;

    // Define the href of the different pages of the site
    var hrefHome = '[href="' + homeUrl + '/"]' ;
    var hrefBlog = '[href="' + homeUrl + '/blog/"]' ;
    var hrefRealisations = '[href="' + homeUrl + '/realisations/"]' ;

    // To be deleted
    console.log('homeUrl = ' + homeUrl) ;
    console.log('fullUrl = ' + fullUrl);
    console.log('path = ' + path) ;
    console.log('href = ' + hrefHome + ' hrefBlog = ' + hrefBlog + ' hrefRealisations = ' + hrefRealisations) ;

    // Add active class to the current item of the menu
    switch(path) {
        case '/' :
            $(hrefHome).addClass('active');
            break ;
        case '/blog/' :
            $(hrefBlog).addClass('active');
            break ;
        case '/realisations/' :
            $(hrefRealisations).addClass('active');
            break ;
    } ;

    // AJAX to get the url of the other pictures of a project
    $('img.image').on('click', function(){
        // get the title and add it to the modal
        var title = $(this).attr("alt") ;
        $('#ModalLabel').text(title) ;

        // ajax to get the other images url and add them to the modal carousel
        jQuery.post(
            ajaxurl,
            {
                'action': 'neos_modal',
                'title': title
            },
            function(response){
                var html = '' ;
                var result = JSON.parse(response) ;
                var i = 1 ;
                result.forEach(function(obj) {
                    var id = 'image-modal-' + i ;

                    var htmlDiv = '<div id="' + id + '"></div>' ;

                    i += 1 ;
                    $('.slick-slider').append(htmlDiv) ;
                    var htmlImg = '<img src="' + obj.img_imageurl + '">' ;
                    var idToFind = '#' + id ;
                    $(idToFind).append(htmlImg) ;
                })
            }
        ).done(function(){
            $('.variable-width').slick({
                dots: true,
                infinite: true,
                autoplay: true,
                speed: 1000,
                slidesToShow: 1,
                centerMode: true,
                variableWidth: true
            });
            $("#articleModal").modal();
        });

    }) ;  // end of on click

    // delete added elements when closing the modal
    $('#articleModal').on('hidden.bs.modal', function () {
        $('.slick-slider').html('') ;
        $('.slick-slider').removeClass('slick-initialized') ;
        $('.slick-slider').removeClass('slick-dotted') ;
        $('#ModalLabel').text('') ;
    });
}) ; // end of Jquery


// <div class="carousel-item active">
//   <img class="d-block w-100 img-fluid" src="./img/IMG_3136.jpg" alt="First slide">
//  </div>
// <div class="carousel-item">
//   <img class="d-block w-100 img-fluid" src="./img/IMG_3136.jpg" alt="First slide">
//  </div
/*
function(response){
var html = '' ;
var result = JSON.parse(response) ;
var i = 1 ;
result.forEach(function(obj) {
    var id = 'image-modal-' + i ;

    if ( i == 1 ) {
        var htmlDiv = '<div class="carousel-item active" id="' + id + '"></div>' ;
    } else {
        var htmlDiv = '<div class="carousel-item" id="' + id + '"></div>' ;
    }

    i += 1 ;
    $('#carousel-modal').prepend(htmlDiv) ;
    var htmlImg = '<img class="d-block w-100 img-fluid" src="' + obj.img_imageurl + '">' ;
    var idToFind = '#' + id ;
    $(idToFind).append(htmlImg) ;
})
}
).done(function(){
    $("#articleModal").modal();
    console.log('done');
});>
*/