<?php

?>
<div class="col-12 col-sm-6 blog-div">
	<?php if(has_post_thumbnail()) {
		the_post_thumbnail('homepage-thumb', ['class' => 'img-fluid']);
	}; ?>
	<h6><?php the_title(); ?></h6>
	<p><?php the_content($more='<br>Lire plus...'); ?></p>
</div>