<?php
/**
 * Created by PhpStorm.
 * User: Alexandre
 * Date: 3/6/2018
 * Time: 11:08 AM
 */
/* Template Name: Post */
get_header();

?>

<header class="header-area" id="sticky-header">
	<nav class="navbar navbar-expand-lg navbar-light">
		<a class="navbar-brand" href="<?= get_site_url(); ?>"><img src="<?= get_template_directory_uri();?>/img/logo_didrgj_c_scale,w_200.png" alt=""></a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
			<span class="navbar-toggler-icon"></span>
		</button>

		<?php wp_nav_menu( array(
			'menu' => 'innova_menu',
			'theme_location' => 'header-menu',
			'container' => 'div',
			'container_class' => 'collapse navbar-collapse justify-content-end',
			'container_id' => 'collapsibleNavbar',
			'menu_class' => 'navbar-nav'
		) ); ?>

	</nav>
	<div class="row blogTitle">
		<h2 class="center">Blog</h2>
	</div>
</header>
<div class="container-fluid">
	<div class="row content blog-container">
		<div class="col-12 col-md-3 categories">
			<h4>Catégories</h4>
			<ul>
				<?php $categories = (get_categories());
				foreach ($categories as $key=>$category) : ?>
					<li><a href="<?= get_site_url() . '/blog/?cat=' . $category->name; ?>"><?= $category->name; ?> (<?= $category->count ?>)</a></li>
				<?php endforeach; ?>
			</ul>
		</div>
		<div class="col-12 col-md-9">
			<div class="row">
				<!-- Reapeat blog -->
				<?php while (have_posts()) : the_post(); ?>
							<div class="col-12 blog-div">
								<?php if(has_post_thumbnail()) : the_post_thumbnail('single-thumb', ['class' => 'img-fluid img-blog']); endif; ?>
								<h5><?= the_title(); ?></h5>
								<p><?= the_content(); ?></p>
							</div>
				<?php endwhile; ?>
				<!-- End Reapeat blog -->
			</div>
		</div>
	</div>
	<div class="row btn-other-blog col-12 col-md-9 justify-content-around">
		<?php previous_posts_link( 'Articles précédents' ); ?>
		<?php next_posts_link( 'Articles suivants' ); ?>
	</div>
</div>

<?php get_footer(); ?>