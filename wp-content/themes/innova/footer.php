<footer>
	<div class="container-fluid">
		<div class="row">
			<div class="col-12 col-md-2 copyright">
                <img src="<?= get_template_directory_uri()?>/img/logo.svg" alt="" class="logo-footer">
				<p>copyright, <?= date('Y'); ?></p>
			</div>
			<div class="col-12 col-md-6 footer-links">
				<div class="col-12 col-md-4">
					<ul>
						<li><a href="<?= get_site_url(); ?>#introduction">Qui sommes-nous ?</a></li>
						<li><a href="<?= get_site_url(); ?>/realisations/">Nos réalisations</a></li>
						<li><a href="<?= get_site_url(); ?>/blog/">Blog</a></li>
                        <li><a href="<?= get_site_url(); ?>#contact">Contact</a></li>
					</ul>
				</div>
				<div class="col-12 col-md-2">
					<div class="social-media">
						<a href="https://www.facebook.com/luxinnova.lu/"><i class="fab fa-facebook"></i></a>
						<!-- other links...
						<a href=""><i class="fab fa-twitter"></i></a>
						<a href=""><i class="fab fa-instagram"></i></a> -->
					</div>
				</div>
                <div class="col-md-6">
                    <ul class="companyInfo">
                        <li>INNOVA Luxembourg SARL</li>
                        <li>37, Rue de la Gare | L-3377 Leudelange</li>
                        <li>Aut.Ministérielle n° 10015702/0/1/2/6/7</li>
                        <li>N° TVA LU25071502</li>
                        <li>N° Registre du commerce : B212020</li>
                    </ul>
                </div>
			</div>
			<div class="col-12 col-md-3 footer-labels">
				<div class="row">
					<div class="text-center label">
						<img src="<?= get_template_directory_uri()?>/img/lux-t.png" class="img-fluid">
					</div>
					<div class="text-center label">
						<img src="<?= get_template_directory_uri()?>/img/seal-t.png" class="img-fluid">
					</div>

					<div class="text-center label">
						<img src="<?= get_template_directory_uri()?>/img/membre.jpg"class="img-fluid">
					</div>
					<div class="text-center label">
						<img src="<?= get_template_directory_uri()?>/img/EFZ.png" class="img-fluid">
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>

<!-- Modal -->
<div class="modal fade" id="articleModal" tabindex="-1">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="ModalLabel"></h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="container">
                    <div class="slick-slider variable-width"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END Modal -->
<?php wp_footer();  ?>
<script type="text/javascript" src="<?= get_template_directory_uri()?>/slick-1.8.0/slick/slick.js"></script>
</body>
</html>
