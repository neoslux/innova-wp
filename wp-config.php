<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'innova');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'zKSR8sdZ+o!{(TioNhiLgqxoz;d1Q3aLp9+Gn=;M7ZgM):fT-`S<uk_~}65sM5Eg');
define('SECURE_AUTH_KEY',  'Q%Is5h-PtP`6u=A{8IMXuCf,<BJj[EYboL*nslLQgZ@sq_n<h{t@- 8]{ 59I3D@');
define('LOGGED_IN_KEY',    't~}cH#k%^dqzt.LqgwN=kVOd)!2<pkBN4X[?lui1wk&H@%R5,t*7chC*.}> -+uv');
define('NONCE_KEY',        '(x>^|-emHqd{d;LryNJP{YhK`bAW0ok&R~9RqOqB1m6c*%xpxx)hAha>Z0MEZn@I');
define('AUTH_SALT',        '(Lhlr_G@pj< E=+UY%KNaxlu:$@?VPVhm P[7wXU%6o1%<BoI^W?S=6y!e*q5+;i');
define('SECURE_AUTH_SALT', 'KoB)o7a},KhfdI>o30k k`p(E6[qswB$bH*}L6|?>_eL+c{v,tuxE6;yaUU0cW*]');
define('LOGGED_IN_SALT',   '-]d_mI@?)W75daN=?BE6(I-fJgQ/8O(HDTYDF7bM*`S; |sY~hX!/eF|;2^?iTVr');
define('NONCE_SALT',       'R2$DxqXOzCHdG/V=+yZT>Nx2sO$UWP0Fu_6+n}8x(K`?Ahdsf1>=t;eLjX$(Ji-q');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
